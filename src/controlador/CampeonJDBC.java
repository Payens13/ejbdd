package controlador;

import modelo.Campeon;

import java.sql.*;
import java.util.ArrayList;

public class CampeonJDBC {
    private final String SELECT_ALL = "SELECT * FROM Campeones";
    private final String SELECT_BY_ID = "SELECT * FROM Campeones WHERE id = ?";
    private final String SELECT_BY_NOMBRE = "SELECT * FROM Campeones WHERE nombre = ?";
    private final String SELECT_MAX_ID = "SELECT MAX(id) AS max_id FROM Campeones";
    private final String SELECT_MIN_ID = "SELECT MIN(id) AS min_id FROM Campeones";
    private final String SELECT_AVG_ID = "SELECT AVG(id) AS avg_id FROM Campeones";
    private final String SELECT_SUM_ID = "SELECT SUM(id) AS sum_id FROM Campeones";
    private final String INSERT_CAMPEON = "INSERT INTO Campeones (Nombre, Rol, Region) VALUES(?, ?, ?)";
    private final String DELETE_CAMPEON_BY_ID = "DELETE FROM Campeones WHERE id = ?";
    private final String UPDATE_CAMPEON_BY_ID = "UPDATE Campeones SET Nombre = ?, Rol = ?, Region = ? WHERE id = ?";
    private final String UPDATE_NOMBRE_BY_ID = "UPDATE Campeones SET Nombre = ? WHERE id = ?";

    Connection con;

    public CampeonJDBC(Connection con) {
        this.con = con;
    }

    public ArrayList<Campeon> select_all() {
        ArrayList<Campeon> lista_campeones = new ArrayList<Campeon>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_ALL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String rol = rs.getString("rol");
                String region = rs.getString("region");
                Campeon c = new Campeon(id, nombre, rol, region);
                lista_campeones.add(c);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lista_campeones;
    }

    public Campeon select_by_id(int id) {
        Campeon c = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                String nombre = rs.getString("nombre");
                String rol = rs.getString("rol");
                String region = rs.getString("region");
                c = new Campeon(id, nombre, rol, region);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    public ArrayList<Campeon> select_by_nombre(String nombre) {
        ArrayList<Campeon> lista_campeones = new ArrayList<Campeon>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_BY_NOMBRE);
            stmt.setString(1, nombre);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String rol = rs.getString("rol");
                String region = rs.getString("region");
                Campeon c = new Campeon(id, nombre, rol, region);
                lista_campeones.add(c);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return lista_campeones;
    }

    public int select_max_id() {
        int max_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_MAX_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                max_id = rs.getInt("max_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return max_id;
    }

    public int select_min_id() {
        int min_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_MIN_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                min_id = rs.getInt("min_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return min_id;
    }

    public double select_avg_id() {
        double avg_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_AVG_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                avg_id = rs.getDouble("avg_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return avg_id;
    }

    public int select_sum_id() {
        int sum_id = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_SUM_ID);
            rs = stmt.executeQuery();
            if (rs.next()) {
                sum_id = rs.getInt("sum_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return sum_id;
    }


    public boolean insert_campeon(Campeon campeon) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(INSERT_CAMPEON);
            stmt.setString(1, campeon.getNombre());
            stmt.setString(2, campeon.getRol());
            stmt.setString(3, campeon.getRegion());
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public boolean delete_campeon_by_id(int id) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(DELETE_CAMPEON_BY_ID);
            stmt.setInt(1, id);
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public boolean update_campeon_by_id(int id, Campeon campeon) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_CAMPEON_BY_ID);
            stmt.setString(1, campeon.getNombre());
            stmt.setString(2, campeon.getRol());
            stmt.setString(3, campeon.getRegion());
            stmt.setInt(4, id);
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }

    public boolean update_nombre_by_id(int id, String nombre) {
        boolean salida = true;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(UPDATE_NOMBRE_BY_ID);
            stmt.setString(1, nombre);
            stmt.setInt(2, id);
            int x = stmt.executeUpdate();
            if (x != 1) {
                salida = false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return salida;
    }
}
