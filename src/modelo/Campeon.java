package modelo;

public class Campeon {
    private int id;
    private String nombre;
    private String rol;
    private String region;

    public Campeon(int id, String nombre, String rol, String region) {
        this.id = id;
        this.nombre = nombre;
        this.rol = rol;
        this.region = region;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "Campeon{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", rol='" + rol + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
