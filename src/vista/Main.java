package vista;

import Utilidades.Conexion;
import controlador.CampeonJDBC;
import modelo.Campeon;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Conexion c = new Conexion();
        Connection conn = null;
        try {
            System.out.println("Connecting to database...");
            conn = c.getConnection();
            CampeonJDBC controller = new CampeonJDBC(conn);

            Scanner sc = new Scanner(System.in);
            String opcion = "";
            while (!opcion.equals("9")) {
                System.out.println("\nPor favor, introduce una opción:");
                System.out.println("1. Mostrar todos los campeones");
                System.out.println("2. Mostrar el campeón con ID 1");
                System.out.println("3. Mostrar los campeones con nombre 'Ahri'");
                System.out.println("4. Mostrar el ID más grande");
                System.out.println("5. Insertar un nuevo campeón");
                System.out.println("6. Actualizar el nombre del campeón con ID 999");
                System.out.println("7. Actualizar todos los parámetros de un campeón por su ID");
                System.out.println("8. Borrar el campeón por ID");
                System.out.println("9. Salir");
                opcion = sc.nextLine();

                switch (opcion) {

                    case "1":
                        // Mostrar todos los datos de la tabla Campeones
                        System.out.println("Mostrando todos los campeones:");
                        ArrayList<Campeon> lista = controller.select_all();
                        if (lista == null || lista.isEmpty()) {
                            System.out.println("No se encontraron campeones.");
                        } else {
                            for (Campeon champ : lista) {
                                System.out.println(champ);
                            }
                        }
                        break;

                    case "2":
                        // Mostrar los datos de un campeón por su ID
                        System.out.println("\nIntroduce la ID del campeón que quieres mostrar:");
                        int idMostrar = sc.nextInt();
                        sc.nextLine();
                        Campeon campeonPorId = controller.select_by_id(idMostrar);
                        if (campeonPorId == null) {
                            System.out.println("No se encontró el campeón con ID " + idMostrar + ".");
                        } else {
                            System.out.println(campeonPorId);
                        }
                        break;

                    case "3":
                        // Mostrar los datos de los campeones por su nombre
                        System.out.println("\nMostrando los campeones con nombre 'Ahri':");
                        ArrayList<Campeon> campeonesPorNombre = controller.select_by_nombre("Ahri");
                        if (campeonesPorNombre == null || campeonesPorNombre.isEmpty()) {
                            System.out.println("No se encontraron campeones con el nombre 'Ahri'.");
                        } else {
                            for (Campeon champ : campeonesPorNombre) {
                                System.out.println(champ);
                            }
                        }
                        break;
                    case "4":

                        // Mostrar el ID más grande en la tabla Campeones
                        System.out.println("\nEl ID más grande es: " + controller.select_max_id());
                        break;

                    case "5":
                        // Insertar un nuevo campeón en la tabla Campeones
                        System.out.println("\nInsertando un nuevo campeón:");
                        Campeon nuevoCampeon = new Campeon(999, "NuevoCampeon", "Asesino", "Ionia");
                        boolean insertado = controller.insert_campeon(nuevoCampeon);
                        if (insertado) {
                            System.out.println("El campeón ha sido insertado correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al insertar el campeón.");
                        }
                        break;

                    case "6":
                        // Actualizar parámetro: Actualizar el nombre del campeón con ID 999
                        System.out.println("\nIntroduce la ID del campeón cuyo nombre quieres actualizar:");
                        int idActualizarNombre = sc.nextInt();
                        sc.nextLine();
                        System.out.println("Introduce el nuevo nombre:");
                        String nuevoNombre = sc.nextLine();
                        boolean nombreActualizado = controller.update_nombre_by_id(idActualizarNombre, nuevoNombre);
                        if (nombreActualizado) {
                            System.out.println("El nombre del campeón ha sido actualizado correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al actualizar el nombre del campeón.");
                        }
                        break;

                    case "7":
                        //actualizar todos los parámetros de un campeón por su ID
                        Campeon campeonActualizado = new Campeon(999, "CampeonActualizado", "Tanque", "Demacia");
                        boolean actualizado = controller.update_campeon_by_id(999, campeonActualizado);
                        if (actualizado) {
                            System.out.println("El campeón ha sido actualizado correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al actualizar el campeón.");
                        }
                        break;

                    case "8":
                        //Borrar por clave primaria (ID)
                        System.out.println("Introduce la ID del campeón que quieres borrar:");
                        int idBorrar = sc.nextInt();
                        sc.nextLine();
                        boolean borrado = controller.delete_campeon_by_id(idBorrar);
                        if (borrado) {
                            System.out.println("El campeón ha sido borrado correctamente.");
                        } else {
                            System.out.println("Ha ocurrido un error al borrar el campeón.");
                        }
                        break;

                    case "9":
                        System.out.println("Saliendo...");
                        break;
                    default:
                        System.out.println("Opción no reconocida.");
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        System.out.println("-20 LPs. Cerrando sesión...");
    }
}
